# Script para backup de banco de dados, sites e arquivos

* Script para realizar backup da(s) base(s) de dados, sites e arquivos de configuração.

* Criar no /root o arquivo backup.sh e permissao 755.

  touch backup.sh

  chmod 755 backup.sh

* Agendar no crontab a execucao do backup

  crontab -u root -e

  01 0 * * * /root/site-bd-files-backup.sh

* Instalar o(s) pacote(s): zip

  apt install zip
