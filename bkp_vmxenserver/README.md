# Script para backup de VM's XCP-NG / Xenserver

* Alterar o parametro UUID da area de armazenamento

* Alterar o parametro UUID da VM para backup
	* Caso exista mais de uma VM para backup criar nova linha e altear o contator e UUID

* Criar agendamento no cron para automatizar

	crontab -u root -e
	
	01 0 * * 7 /root/bkpvmxenserver.sh
